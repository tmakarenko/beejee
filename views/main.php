<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Task</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		  	<div class="col-md-8 col-md-offset-2">
			    <div class="navbar-header">
			      <a class="navbar-brand" href="/">Task</a>
			    </div>
			    <ul class="nav navbar-nav">
			      <li><a href="/">Create Task</a></li>
			      <li><a href="/tasks">Show task</a></li>
			      <li><a href="/admin">Admin</a></li>
			      <?php 
					$db = new \Delight\Db\PdoDsn('mysql:dbname=endzait;host=mysql.zzz.com.ua;charset=utf8mb4', 'endzait', 'Trayzer13');
			        $auth = new \Delight\Auth\Auth($db);
			        if($auth->isLoggedIn()){
			        	echo '<li><a href="/">Logged as '.$auth->getEmail()."</a></li>";
						echo '<script type="text/javascript">var isAdmin=true;</script>';
					}
	 			  ?>
			    </ul>
			</div>
		  </div>
		</nav>
	</header>
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="  crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</body>
</html>