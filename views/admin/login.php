<?php require_once($_SERVER['DOCUMENT_ROOT'].'/views/main.php'); ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h2>Админка</h2>
			<form action="/admin/login" method="POST" enctype="multipart/form-data" id="task-form">
				
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" id="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control" required>
				</div>
				<input type="submit" class="btn btn-primary">
		</div>
	</div>
</div>