<?php require_once($_SERVER['DOCUMENT_ROOT'].'/views/main.php'); 

?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table table-bordered" id="tasks">
				<thead>	
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Email</th>
						<th>Text</th>
						<th>Img</th>
						<th>Is_active</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<div id="dialog-confirm" >
  
</div>
<script type="text/javascript">
	function updateText(rowId){
			$('#dialog-confirm').empty();
	    	$('#dialog-confirm').append('<input id="update-text" type="text" value=""/>');
		    $( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height: "auto",
		      width: 400,
		      modal: true,
		      buttons: {
		        "Update text": function() {
		          	

		          	$.ajax({
					  method: "POST",
					  url: "/api/tasks/update",
					  data: {id:rowId,text:$('#update-text').val()}
					})
					.done(function( msg ) {
					  
					});
					$( this ).dialog( "close" );
                  
		        },
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
	    	});
		}


	function disable(rowId){
		$.ajax({
			method: "POST",
			url: "/api/tasks/disable",
			data: {id:rowId}
		})
		.done(function( msg ) {
		  
		});
	}

	$(document).ready(function() {
	    $('#tasks').DataTable( {
	        "ajax": "/api/tasks",
	        "createdRow": function ( row, data, index ) {
	            if ( data.is_active == 0) {
	                $('td', row).css('background-color','grey');
	            }
        	},
	        "columns": [
	            { "data": "id" },
	            { "data": "name" },
	            { "data": "email" },
	            { "data": "text" },
	            { "data": "img",
	              "render":function ( data, type, row ) {
        			return '<img style="width:160px; height:120px" src="'+ data+'"></img>';
    				}
	             },
	            { "data": "is_active",
	              "render":function ( data, type, row ) {
	              	
	              	
	              	if (typeof isAdmin !== 'undefined'){
		              	if(data == 1){
		              		return '<input type="button" class="btn btn-warning" value="disable" onclick="disable('+row.id+')" data="'+data+'"/>'+' <input type="button" class="btn btn-warning" value="edit" onclick="updateText('+row.id+')"/>';
		              	}else{
		              		return '<input type="button" class="btn btn-warning" value="edit" onclick="updateText('+row.id+')"/>';
		              	}
		            }else{
		            	return data;
		            }
		            
        			
    			  }
	            }
	        ],
	        "lengthMenu":[3]
	    } );

	    





	} );
</script>
