<?php require_once($_SERVER['DOCUMENT_ROOT'].'/views/main.php'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h2>Добавить задание</h2>
			<form action="/api/tasks/create" method="POST" enctype="multipart/form-data" id="task-form">
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" id="name" class="form-control">
				</div>	
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" id="email" class="form-control">
				</div>
				<div class="form-group">
					<label for="text">Text</label>
					<input type="text" name="text" id="text" class="form-control">
				</div>
				<div class="form-group">
					<label for="img">Image</label>
					<input type="file" name="img" id="img" class="form-control" data-validation="dimension" data-validation-dimension="max320x240">
				</div>
				<input type="hidden" value="1" name="is_active">
				<input type="submit" class="btn btn-primary"  value="submit">
				<input type="button" class="btn btn-primary"  value="Пред просмотр" onclick="preview()">				
			</form>
			<div id="preview" style="opacity: 0">
				Image:  <img id="preimg" src=""/ style="width:160px; height:120px">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$("#task-form").validate({              
              rules: {
                email: {
                  required: true,
                  email: true
                },
                text:{
                    required:true,
                },
                img:{
                  required: true,
                  extension: "png|gif|jpg",
                  accept:"image/*"
                }
              },
              messages:{
                text:"Укажите текст задания",
                email: {
                  required: "Укажите почту",
                  email: "Укажите почту в правильном формате"
                },
                img:{
                  required: "Внесите файл фото",
                  extension: "Фото должно быть в формате картинки (png,gif,jpg)",
                  accept:"Формат файла должен быть фото"
                },
               submitHandler: function(form) {
      			form.submit();
    		   }
              }    
        });
	
});

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preimg').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#img").change(function () {
        readURL(this);
    });

function preview(){
	$('#preview').append("<p>Name: "+$('#name').val()+"</p>");
	$('#preview').append("<p>Email: "+$('#email').val()+"</p>");
	$('#preview').append("<p>Text: "+$('#text').val()+"</p>");
	$("#preview").animate({opacity:'1'}, 400);
}
</script>
