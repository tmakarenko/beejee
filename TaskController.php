<?php
namespace MVC;
use MVC\Task;

class TaskController {
 
    
 	
    public function createView(){
    	View::output([], "./views/task/create.php");
    }

    public function create(){
    	$task = new Task($_REQUEST);
    	$task->save();
        header('Location: http://local1.loc/tasks');
    }

    public function showAll(){
        
    	View::output([], "./views/task/show.php");
    }

	public function getTasks(){
    	echo Task::getAll();
    }

    public function updateTask(){
       Task::update($_POST['id'],$_POST['text']);
       header('Location: http://local1.loc/tasks');
    }

    public function disableTask(){
        Task::disable($_POST['id']);
        header('Location: http://local1.loc/tasks');
    }    

}

?>