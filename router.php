<?php
namespace MVC;

Class Router{

	protected $controller;
	protected $method;

	private $data = [
		'/' => "MVC\TaskController@createView",
		'/api/tasks/create' => "MVC\TaskController@create",
		'/tasks' => "MVC\TaskController@showAll",
		'/api/tasks' => "MVC\TaskController@getTasks",
		'/api/tasks/update' => "MVC\TaskController@updateTask",
		'/api/tasks/disable' => "MVC\TaskController@disableTask",
		'/admin' => "User\UserController@loginView",
		'/admin/reg' => "User\UserController@reg",
		'/admin/login' => "User\UserController@login"
	];

	public function __construct(){
		
		if(strpos($_SERVER['REQUEST_URI'],'?')){
			$route = substr($_SERVER['REQUEST_URI'],0,strpos($_SERVER['REQUEST_URI'],'?'));
		}else{
			$route = $_SERVER['REQUEST_URI'];
		}
		
	
		foreach($this->data as $key=>$value){
			if ($route == $key) {
				$this->controller = substr($this->data[$key],0,strpos($this->data[$key],'@')); 
				$this->method = substr($this->data[$key],strpos($this->data[$key],'@')+1);
				break;
			}
		}
		
	}

	public function run(){
		$conn = new $this->controller();
		$me = $this->method;
		$conn->$me();
	}

}



?>