<?php
namespace MVC;
require "vendor/autoload.php";
use \PDO;
use \FluentPDO;
use \Imagick;


class Task {
    public $name;
    public $email;
 	public $text;
 	public $img;
    public $is_active;


    public function __construct(Array $data) {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->text = $data['text'];
        
        $imagick = new \Imagick($_FILES["img"]["tmp_name"]);
        $imagick->resizeImage(320,240,Imagick::FILTER_LANCZOS,1);
        $imagick->writeImage($_SERVER['DOCUMENT_ROOT']."/images/".$_FILES['img']['name']);
        $imagick->destroy();

        $this->img = "/images/".$_FILES['img']['name'];
        $this->is_active = $data['is_active'];
    }

    public function save(){
    	$pdo = new PDO('mysql:dbname=endzait;host=mysql.zzz.com.ua;charset=utf8mb4', 'endzait', 'Trayzer13');
		$fluent = new FluentPDO($pdo);
    	$fluent->insertInto('task',[
    		'name' => $this->name,
    		'email' => $this->email,
    		'text' => $this->text,
    		'img' => $this->img,
    		'is_active' => $this->is_active
    	])->execute();

    }

    public static function getAll(){
    	$pdo = new PDO('mysql:dbname=endzait;host=mysql.zzz.com.ua;charset=utf8mb4', 'endzait', 'Trayzer13');
 		$fluent = new FluentPDO($pdo);
 		$res = $fluent->from('task')->fetchAll(); 
		return json_encode(["data"=>$res],JSON_UNESCAPED_UNICODE);    	
    }

    public static function update($id,$text){
        $pdo = new PDO('mysql:dbname=endzait;host=mysql.zzz.com.ua;charset=utf8mb4', 'endzait', 'Trayzer13');
        $fluent = new FluentPDO($pdo);
        $fluent->update('task')->set(['text' => $text])->where('id',$id)->execute();
    }

    public static function disable($id){
        $pdo = new PDO('mysql:dbname=endzait;host=mysql.zzz.com.ua;charset=utf8mb4', 'endzait', 'Trayzer13');
        $fluent = new FluentPDO($pdo);
        echo $fluent->update('task')->set(['is_active' => 0])->where('id',$id)->execute();
    }

}

?>